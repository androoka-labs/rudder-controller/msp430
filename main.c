
#include <msp430.h>

/* this does nothing logical except to 
   confirm the headers and linker scripts 
   are sourced in the make script
   and a elf file is created. */

int main() {
  int a=1000;
  while(P4IN & BIT2) {
    a--;
  }

  TB0CCTL0 = 0;

  return 0;
}