

# Smart Rudder-Controller

## Description
Kayaking without the ability to manually control a rudder due to permanent disabilities involves extra effort to maintain direction of travel of the kayak. Without a rudder, a kayak is easily pushed in the wind and it requires additional effort to steer the kayak in addition to forward movement.  
This controller is designed to 'predict' the intended direction of travel and provide assistance in the form of a servo controlled rudder. It will do this without any direct input - using the 9DOF sensor to predict the intent and provide assistance to maintain the intended direction.

[MSP430FR5994](https://www.ti.com/product/MSP430FR5994)  
[ICM-20948](https://invensense.tdk.com/products/motion-tracking/9-axis/icm-20948/)  
[GOTeck GB3506MG Brushless Servo IP67 Rated 35kg](https://hobbyking.com/en_us/goteck-gb3506mg-metal-geared-180-high-torque-hv-digital-brushless-servo-35kg-0-06sec-75g.html)  

### Design
The design of the controller is to use a 9DOF sensor to detect paddle movement of the kayaker. Normally a direction of travel is controlled by the differential paddle (push harder on port paddle and kayak turns left). In parallel to this, the kayaker generally leans into the turn in order which assists the kayak by having additional drag on the turn side and less drag on for outward side.

#### Detection of differential paddles
As the kayaker turns, additional effort is put into the stroke which results in a longer time per stroke on the outer direction and less time per stroke on the inner direction. The larger effort will obviously yaw the boat during the stroke, while the lesser stroke will generally not.  

The controller will need to detect the stroke by   
a) acceleration of the boat (acceleration sensor).  
b) yaw of the boat (gyro sensor)  
c) the residual resultant yaw (magnetic direction sensor)  
  
The controller should also detect repeat stokes and determine if it resulted in a turn OR if it was simply correcting for yaw of the kayak possible due to current or winds (kayaker correcting for yaw or did they intend to turn the boat).  

Using all 9DOF sensors should result in a good potential to determine the 'intent' direction without direct input. For instance if the kayak is paddled left, the rudder will steer left after several strokes.  

If sufficient controller power and rudder strength is available, the rudder could also be used to 'swim' the boat by swinging side to side to create forward momentum.

#### Additional Project
Paddling a kayak without using the ability to push the legs forward during the stokes results in more excersion than an able bodies. It may be possible to link the rudder to a small assistance 'trolling' motor which would allow people with disabilities to keep up with able-bodies kayakers and go longer distance before general exhaustion.

