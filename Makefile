
mmcu=msp430fr5994
srcs = main.c
cflags = -I /opt/ti/msp430-gcc/include
ldflags = -L /opt/ti/msp430-gcc/include
gcc = /opt/ti/msp430-gcc/bin/msp430-elf-gcc

rudder-controller.elf: $(srcs)
	$(gcc) $(srcs) -mmcu=$(mmcu) $(cflags) $(ldflags) -o$@

 